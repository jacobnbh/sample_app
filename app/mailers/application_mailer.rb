class ApplicationMailer < ActionMailer::Base
  default from: 'meiyoureply@hotmail.com'
  layout 'mailer'
end
